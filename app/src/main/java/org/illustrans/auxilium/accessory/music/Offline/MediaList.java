package org.illustrans.auxilium.accessory.music.Offline;


public class MediaList {
    private int id;
    private String artist;
    private String title;
    private String path;
    private String album;

    public MediaList(int id, String artist, String title, String path, String album) {
        this.id = id;
        this.artist = artist;
        this.title = title;
        this.path = path;
        this.album = album;
    }

    public MediaList() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }
}
