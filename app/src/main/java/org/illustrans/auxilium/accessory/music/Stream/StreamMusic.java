package org.illustrans.auxilium.accessory.music.Stream;

import android.media.AudioManager;
import android.media.MediaPlayer;

import org.illustrans.auxilium.accessory.music.SongObject;
import org.illustrans.naturallanguageprocessing.util.TextUtility;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by adart on 02/12/2017.
 */

/*
http://j.ginggong.com/jOut.ashx?k=vet%20mua&h=mp3.zing.vn&code=54db3d4b-518f-4f50-aa34-393147a8aa18
*/
public class StreamMusic {
    /**
     * const string
     */


    private final static String apiHost = "http://j.ginggong.com/jOut.ashx?";
    private final static String searchKey = "&k=";
    private final static String streamSrcKeeng = "&h=keeng.vn";
    private final static String streamSrcZing = "&h=mp3.zing.vn";
    private final static String apiCode = "&code=54db3d4b-518f-4f50-aa34-393147a8aa18";

    private final static float MaxVolumevalue = (float) 0.9;
    private final static float MinVolumevalue = (float) 0.0;
    /**
     * class element
     */
    private String key_words = "";
    private List<SongObject> songObjects = new ArrayList<>();
    private MediaPlayer mPlayer;

    /**
     * Constructor
     */
    public StreamMusic() {
        mPlayer = new MediaPlayer();
    }

    public StreamMusic(String keywords) {
        mPlayer = new MediaPlayer();
        key_words = keywords;
    }


    /**
     *
     */
    //http://j.ginggong.com/jOut.ashx?k=vets%20muaw&h=mp3.zing.vn&code=54db3d4b-518f-4f50-aa34-393147a8aa18

    /*
    * TODO: Get server response
    * */
    private void FindStreamURL() {
        key_words = TextUtility.toVNnonSign(key_words);
        String url = apiHost + streamSrcZing + apiCode + searchKey + key_words.replaceAll(" ", "%20");

        StreamFinder streamFinder = new StreamFinder(url);
        streamFinder.StartFindStreamURL();

        /**
         * Wait for AsyncTask complete
         */
        while (streamFinder.songObject_list.isEmpty()) ;

        /**
         * Clone to songObjects
         */
        songObjects = new ArrayList<>(streamFinder.songObject_list);

    }

    public void Prepare() {
        this.FindStreamURL();


        mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

        try {
            mPlayer.setDataSource(songObjects.get(0).UrlJunDownload);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            mPlayer.prepare();

        } catch (IllegalStateException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public void Stream() {
        mPlayer.start();
    }

    public void ChangeVolume(float leftVolume, float rightVolume) {

        leftVolume = (leftVolume > MaxVolumevalue) ? MaxVolumevalue : leftVolume;
        leftVolume = (leftVolume < MinVolumevalue) ? MinVolumevalue : leftVolume;

        rightVolume = (rightVolume > MaxVolumevalue) ? MaxVolumevalue : rightVolume;
        rightVolume = (rightVolume < MinVolumevalue) ? MinVolumevalue : rightVolume;


        mPlayer.setVolume(leftVolume, rightVolume);
    }

    public void Pause() {
        mPlayer.pause();
    }

    public void Stop() {
        mPlayer.stop();
    }
}
