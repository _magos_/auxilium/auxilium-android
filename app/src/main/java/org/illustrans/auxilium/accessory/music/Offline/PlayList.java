//package org.illustrans.auxilium.accessory.music.Offline;
//
///**
// * Created by adart on 28/12/2017.
// */
//
//
//
//
//import android.Manifest;
//        import android.content.ContentResolver;
//        import android.content.SharedPreferences;
//        import android.content.pm.PackageManager;
//        import android.database.Cursor;
//        import android.os.Build;
//        import android.provider.MediaStore;
//        import android.support.v4.app.ActivityCompat;
//        import android.os.Bundle;
//        import android.view.View;
//        import android.widget.AdapterView;
//        import android.widget.ArrayAdapter;
//        import android.widget.ListView;
//
//
//        import java.util.ArrayList;
//
//public class PlayList {
//
//    private static final int REQUEST_PERMISSIONS = 1;
//    ListView lvSongs;
//    ArrayAdapter<String> adapterSongs;
//    ArrayList<String> listSongs = new ArrayList<>();
//    ArrayList<String> listPath = new ArrayList<>();
//    ArrayList<MediaList> listMedia = new ArrayList<>();
//
////    @Override
//    protected void onCreate(Bundle savedInstanceState) {
////        super.onCreate(savedInstanceState);
////        setContentView(R.layout.activity_music_list);
//
//        //setSystemUI();
//
//        //checkPermissions();
//
//        addControls();
//        addEvents();
//    }
//
//    private void checkPermissions() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            String[] permissions = {
//                    Manifest.permission.READ_EXTERNAL_STORAGE,
//                    Manifest.permission.WRITE_EXTERNAL_STORAGE
//            };
//
//            //requestPermissions(permissions, REQUEST_PERMISSIONS);
//        }
//    }
//
////    private void setSystemUI() {
////        View decorView = getWindow().getDecorView();
////        // Hide both the navigation bar and the status bar.
////        // SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
////        // a general rule, you should design your app to hide the status bar whenever you
////        // hide the navigation bar.
////        int uiOptions = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
////                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
////                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
////                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
////                | View.SYSTEM_UI_FLAG_FULLSCREEN
////                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
////        decorView.setSystemUiVisibility(uiOptions);
////    }
//
//    private void addEvents() {
//        lvSongs.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                String path = listMedia.get(i).getPath();
//                String artist = listMedia.get(i).getArtist();
//                String title = listMedia.get(i).getTitle();
//                String album = listMedia.get(i).getAlbum();
//                int id = listMedia.get(i).getId();
//
//                SharedPreferences prefs = getSharedPreferences("media", MODE_PRIVATE);
//                SharedPreferences.Editor editor = prefs.edit();
//                editor.putString("path", path);
//                editor.putString("artist", artist);
//                editor.putString("title", title);
//                editor.putString("album", album);
//                editor.putInt("id", id);
//                editor.apply();
//
//                finish();
//            }
//        });
//    }
//
//    private void addControls() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
//                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
//                getSongs();
//            }
//        }
//        else {
//            checkPermissions();
//        }
////
////        adapterSongs = new ArrayAdapter<>(
////                PlayList.this,
////                android.R.layout.simple_list_item_1,
////                listSongs
////        );
////        lvSongs = findViewById(R.id.lv_songs);
////        lvSongs.setAdapter(adapterSongs);
//    }
//
//    public void getSongs(){
//        ContentResolver contentResolver = getContentResolver();
//        Cursor songCursor = contentResolver.query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, null, MediaStore.Audio.Media.IS_MUSIC, null, null);
//        int i = 0;
//
//        if (songCursor != null && songCursor.moveToFirst()){
//            int songTitle = songCursor.getColumnIndex(MediaStore.Audio.Media.TITLE);
//            int songArtist = songCursor.getColumnIndex(MediaStore.Audio.Media.ARTIST);
//            int songPath = songCursor.getColumnIndex(MediaStore.Audio.Media.DATA);
//            int songAlbum = songCursor.getColumnIndex(MediaStore.Audio.Media.ALBUM);
//
//            do {
//                String title = songCursor.getString(songTitle);
//                String artist = songCursor.getString(songArtist);
//                String path = songCursor.getString(songPath);
//                String album = songCursor.getString(songAlbum);
//
//                listPath.add(path);
//                listMedia.add(new MediaList(i, artist, title, path, album));
//                listSongs.add(title + "\n " + artist);
//
//                i++;
//            }
//            while (songCursor.moveToNext());
//        }
//
//        if (songCursor != null) {
//            songCursor.close();
//        }
//    }
//}
