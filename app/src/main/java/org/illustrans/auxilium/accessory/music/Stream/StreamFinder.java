package org.illustrans.auxilium.accessory.music.Stream;

import android.os.AsyncTask;

import org.illustrans.auxilium.accessory.music.SongObject;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by adart on 02/12/2017.
 */

public class StreamFinder {

    /**
     *
     */
    public List<SongObject> songObject_list;

    private ConnectAsyncTask conn;

    /**
     *
     */
    public class ConnectAsyncTask extends AsyncTask<Void, Void, List<SongObject>> {
        String data = "";
        String jsonUrl = "";

        public ConnectAsyncTask(String _url) {
            jsonUrl = _url;
        }

        @Override
        protected void onPreExecute() {
            songObject_list = new ArrayList<SongObject>();
        }

        @Override
        protected List<SongObject> doInBackground(Void... voids) {
            try {
                URL url = new URL(jsonUrl);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String line = "";

                while (line != null) {
                    line = bufferedReader.readLine();
                    data = data + line;
                }

                JSONArray jsonArray = new JSONArray(data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = new JSONObject(jsonArray.get(i).toString());

                    String title = jsonObject.get("Title").toString();
                    String artist = jsonObject.get("Artist").toString();
                    String urlJunDownload = jsonObject.get("UrlJunDownload").toString();

                    songObject_list.add(new SongObject(title, artist, urlJunDownload));
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return songObject_list;
        }

        @Override
        protected void onPostExecute(List<SongObject> aVoid) {
            super.onPostExecute(aVoid);

        }
    }

    /**
     * Constructor
     */
    public StreamFinder() {

    }

    public String GetStreamURL(int index) {
        return songObject_list.get(index).UrlJunDownload;
    }

    public SongObject GetStreamObject(int index) {
        return songObject_list.get(index);//.UrlJunDownload;
    }


    public StreamFinder(String url) {
        conn = new ConnectAsyncTask(url);
    }

    public void StartFindStreamURL() {
        conn.execute();
    }
}
