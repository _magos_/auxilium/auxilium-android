package org.illustrans.auxilium.accessory.music;

import org.illustrans.auxilium.accessory.music.Stream.StreamMusic;

import java.util.ArrayList;

/**
 * Created by adart on 28/12/2017.
 */


public class Music {

    /**
     * Volume variable
     */
    private final static float VolumeUnit = (float) 0.1;
    private float CurrentVolume;

    /**
     * Media variable
     */
    private StreamMusic mediaPlayer;
    MusicSource musicSource;

    public enum MusicSource {
        EXTERNAL_STORAGE, STREAM
    }

    public enum MediaResponse {
        WORK_WELL, SERVER_NOT_RESPONDING, NO_SUCH_FILE
    }

    public enum ConstrolCommand {
        play, pause, resume, stop, turnUpVolume, turnDownVolume

    }

    public Music(String songTitle) {
        //set 50 percent volume
        mediaPlayer = new StreamMusic(songTitle);
        CurrentVolume = (float) 0.5;
        mediaPlayer.ChangeVolume(CurrentVolume, CurrentVolume);
    }

    //TODO: Have ability to search by artist name, music type
    public ArrayList<MediaResponse> Build() {
        ArrayList<MediaResponse> mediaResponse = new ArrayList<>();
        /*
        *   Search in external storage first
        */

        /*
        *   Search from Zing mp3
         */
        //StreamMusic mediaPlayer = new StreamMusic(input);


        mediaPlayer.Prepare();

        return mediaResponse;
    }

    public void PlayMusic(ConstrolCommand controlCommand) {

        switch (controlCommand) {
            case play:
            case resume:
                mediaPlayer.Stream();
                break;

            case pause:
                mediaPlayer.Pause();
                break;

            case stop:
                mediaPlayer.Stop();
                break;

            case turnDownVolume:
                CurrentVolume -= VolumeUnit;
                mediaPlayer.ChangeVolume(CurrentVolume, CurrentVolume);
                break;

            case turnUpVolume:
                CurrentVolume += VolumeUnit;
                mediaPlayer.ChangeVolume(CurrentVolume, CurrentVolume);
                break;

            default:
                break;
        }
    }
}
