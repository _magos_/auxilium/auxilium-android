package org.illustrans.auxilium.accessory.music;

/**
 * Created by adart on 04/12/2017.
 */

public class SongObject {
    public String Title = "";
    public String Artist = "";
    public String UrlJunDownload = "";

    public SongObject() {
        Title = "";
        Artist = "";
        UrlJunDownload = "";
    }

    public SongObject(String title, String artist, String url) {
        Title = title;
        Artist = artist;
        UrlJunDownload = url;
    }
}