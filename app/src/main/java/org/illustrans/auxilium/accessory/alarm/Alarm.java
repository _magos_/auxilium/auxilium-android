package org.illustrans.auxilium.accessory.alarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.util.Calendar;

import static android.content.Context.ALARM_SERVICE;

public class Alarm {

    private static final String TAG = Alarm.class.getSimpleName();

    private AlarmManager alarmManager;
    private Intent myIntent;
    private PendingIntent pendingIntent;

    private Context context;

    public Alarm(Context c) {
        context = c;

        alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
        myIntent = new Intent(context, AlarmReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(context, 0, myIntent, 0);
    }

    public String startAlarm(String message) {
        String time[] = message.split(":");
        Log.i(TAG, Integer.valueOf(time[0]) + " " + Integer.valueOf(time[1]));

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, Integer.valueOf(time[0]));
        calendar.set(Calendar.MINUTE, Integer.valueOf(time[1]));
        calendar.set(Calendar.SECOND, 0);

        alarmManager.set(AlarmManager.RTC, calendar.getTimeInMillis(), pendingIntent);

        return "_alarmset#1";
    }
}
