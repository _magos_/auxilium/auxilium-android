package org.illustrans.auxilium.accessory;

import android.content.Context;
import android.content.Intent;

import org.illustrans.auxilium.R;
import org.illustrans.auxilium.accessory.alarm.Alarm;
import org.illustrans.auxilium.accessory.music.Music;

public class Accessory {

    private static final String TAG = Accessory.class.getSimpleName();

    /**
     * accessory stuff
     */
    private onAccessory mListener;
    private Context context;

    /**
     * music stuff
     */
    private Music music;
    private Alarm alarm;

    public Accessory(Context c) {
        context = c;
        if (context instanceof onAccessory) {
            mListener = (onAccessory) context;
        } else {
            throw new RuntimeException(context.toString() + "must implement Accessory");
        }
    }

    public void startProperActivity(String command) {
        String[] selection = null;
        if (command != "") {
            selection = command.split("#");
        } else {
            selection = new String[]{"", ""};
        }

        //TODO  make this shit more simple
        if (selection != null) {
            Intent intent;
            String message = null;
            switch (selection.length) {
                case 0:
                    message = "";

                    break;
                case 1:
                    message = "";

                    break;
                case 2:
                    message = selection[1];

                    break;
                default:
                    message = "";

                    break;
            }

            switch (selection[0]) {
                case "_speak":
                    returnToMainActivity("_speak#" + message);

                    break;
                case "_alarmset":
                    //FIXME make alarm work properly again
                    alarm = new Alarm(context);
                    returnToMainActivity(alarm.startAlarm(message));

                    break;
                case "IOTHOME":
                    returnToMainActivity("IOTHOME#" + message);

                    break;
                case "_weather":
                    returnToMainActivity("_speak#" + context.getString(R.string.TEXTTOSPEECH_WEATHER_SUNNY));

                    break;
                case "_music":
                    if(music != null) {
                        music = null;
                    }
                    music = new Music(message);
                    music.Build();
                    music.PlayMusic(Music.ConstrolCommand.play);
                    returnToMainActivity("_music#ok");

                    break;
                case "_notenough":
                    returnToMainActivity("_speak#" + context.getString(R.string.TEXTTOSPEECH_NOTENOUGH));

                    break;
                default:
                    returnToMainActivity("_dontknow#" + message);

                    break;
            }
        }
    }

    public void returnToMainActivity(String message) {
        if (mListener != null) {
            mListener.onAccessoryResult(message);
        }
    }

    private void PlayMusicListMethod() {

    }

    private void PlaySingleSongMethod(String input) {
        //TODO: get response to notify
    }

    public interface onAccessory {
        void onAccessoryResult(String message);
    }
}
