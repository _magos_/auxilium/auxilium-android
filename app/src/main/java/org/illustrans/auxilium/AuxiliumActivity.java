package org.illustrans.auxilium;

import android.content.Context;
import android.media.AudioManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import org.illustrans.auxilium.accessory.Accessory;
import org.illustrans.cognitive.reaction.Reaction;
import org.illustrans.naturallanguageprocessing.semantics.naturallanguageunderstanding.NaturalLanguageUnderstanding;
import org.illustrans.naturallanguageprocessing.speech.speechtotext.androidgoogleapi.SpeechToText;
import org.illustrans.naturallanguageprocessing.speech.texttospeech.TextToSpeechUmind;
import org.illustrans.naturallanguageprocessing.speech.wakeupword.kitt.ai.snowboy.SnowboyWakeUpWord;

public class AuxiliumActivity extends AppCompatActivity implements SpeechToText.onSpeechToText, NaturalLanguageUnderstanding.onNaturalLanguageUnderstanding, SnowboyWakeUpWord.onWakeUpWord, Accessory.onAccessory {

    private static final String TAG = AuxiliumActivity.class.getSimpleName();

    private SnowboyWakeUpWord snowboyWakeUpWord;
    private TextToSpeechUmind textToSpeechUmind;
    private SpeechToText speechToText;
    private NaturalLanguageUnderstanding naturalLanguageUnderstanding;

    private Reaction reaction;

    private Accessory accessory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        snowboyWakeUpWord = new SnowboyWakeUpWord(this);
        textToSpeechUmind = new TextToSpeechUmind();
        speechToText = new SpeechToText(this);
        naturalLanguageUnderstanding = new NaturalLanguageUnderstanding(this);

        reaction = new Reaction();

        accessory = new Accessory(this);

        setMaxVolume();

        String wellcomeSpeak = "_speak#" + getString(R.string.TEXTTOSPEECH_HELLO);
        startTextToSpeech(wellcomeSpeak);

        startWakeUpWord();
    }

    //-----------------------------------------------------------------
    //wake up word stuff

    public void onWakeUpWordResult() {
        Log.i(TAG, "onWakeUpWordResult");

        startSpeechToText();
    }

    private void startWakeUpWord() {
        snowboyWakeUpWord.start();
    }

    //-----------------------------------------------------------------
    //speech to text stuff

    public void onSpeechToTextResult(String message) {
        Log.i(TAG, "onSpeechToTextResult: " + message);

        startNaturalLanguageUnderstanding(message);
    }

    private void startSpeechToText() {
        speechToText.startSpeechRecognition();
    }

    //-----------------------------------------------------------------
    //natural language understanding stuff

    public void onNaturalLanguageUnderstandingResult(String message) {
        Log.i(TAG, "onNaturalLanguageUnderstandingResult: " + message);

        String[] accessoryCommand = reaction.doReflectReaction(message);
        startAccessory(accessoryCommand);
    }

    private void startNaturalLanguageUnderstanding(String text) {
        naturalLanguageUnderstanding.doNaturalLanguageUnderstanding(text);
    }

    //-----------------------------------------------------------------
    //accessory stuff

    public void onAccessoryResult(String message) {
        Log.i(TAG, "onAccessoryResult: " + message);

        startTextToSpeech(message);
        startWakeUpWord();
    }

    private void startAccessory(String[] command) {
        if (command != null) {
            accessory.startProperActivity(command[0] + "#" + command[1]);
        } else {
            accessory.startProperActivity("");
        }
    }

    //-----------------------------------------------------------------
    //text to speech

    private void startTextToSpeech(String needToSpeech) {
        String[] speech = needToSpeech.split("#");

        String message = null;

        switch (speech[0]) {
            case "_speak":
                message = speech[1];

                break;
            case "_music":
                message = null;

                break;
            case "_alarmset":
                message = getString(R.string.TEXTTOSPEECH_ALARMSET);

                break;
            case "_notenough":
                message = getString(R.string.TEXTTOSPEECH_NOTENOUGH);

                break;
            case "IOTHOME":
                message = getString(R.string.TEXTTOSPEECH_IOTHOME);

                break;
            case "_dontknow":
            case "_weather":
            default:
                message = getString(R.string.TEXTTOSPEECH_DONTKNOW);

                break;
        }

        textToSpeechUmind.speak(message);
    }

    //-----------------------------------------------------------------
    //unspecified stuff

    private void setMaxVolume() {
        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        int maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, maxVolume, 0);
    }
}
