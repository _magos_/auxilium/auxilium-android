package org.illustrans.naturallanguageprocessing.semantics.naturallanguageunderstanding;

import android.content.Context;
import android.os.CountDownTimer;
import android.util.Log;

import org.hpdragon.naturallanguageprocessing.semantics.informationextraction.InformationExtraction;
import org.hpdragon.naturallanguageprocessing.semantics.namedentityrecognition.NamedEntityRecognition;
import org.illustrans.auxilium.util.AppResCopy;
import org.illustrans.naturallanguageprocessing.util.TextUtility;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class NaturalLanguageUnderstanding {

    private static final String TAG = NaturalLanguageUnderstanding.class.getSimpleName();

    private static final boolean OFFLINE_MODE = true;
//    private static final boolean OFFLINE_MODE = false;

    private onNaturalLanguageUnderstanding mListener;

    private NamedEntityRecognition ner;

    private String messageToServer = "";
    private String messageFromServer = "";

    public static final  String EXTRA_MESSAGE = "org.illustrans.MESSAGE";

    public NaturalLanguageUnderstanding(Context context) {
        if (context instanceof onNaturalLanguageUnderstanding) {
            mListener = (onNaturalLanguageUnderstanding) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement onSpeechToText");
        }

//        /*
//        AppResCopy.copyResFromAssetsToSD(context, "namedentityrecognition");
        ner = new NamedEntityRecognition();
        try {
            ner.loadMemory(new File(AppResCopy.DEFAULT_WORK_SPACE + "namedentityrecognition/" + "learned.l"));
        } catch (IOException e) {
            e.printStackTrace();
        }
//        */
    }

    public String getMessageFromServer() {
        return messageFromServer;
    }

    //-----------------------------------------------------------------
    /**
     * communicate server stuff
     */

    private WebSocketClient mWebSocketClient;
    private static final String SERVER_IP = "ws://ucoor2.umind.io";
    private static final int SERVER_PORT = 8080;
    private static final String SERVER_LOGIN_ID = "123788331168";
    private static final int SERVER_WaiTime = 5000;
    private CountDownTimer timer;

    private void connectToUmindServer() {
        URI uri;
        try {
            uri = new URI( SERVER_IP + ":" + SERVER_PORT );
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return;
        }

        mWebSocketClient = new WebSocketClient(uri) {
            @Override
            public void onOpen(ServerHandshake serverHandshake) {
                timer.cancel();
                Log.i(TAG, "Websocket: Opened");
                mWebSocketClient.send("ID#" + SERVER_LOGIN_ID + "#" + messageToServer);
            }

            @Override
            public void onMessage(String s) {
                messageFromServer = s;

                close();
                returnToMainActivity(messageFromServer);
            }

            @Override
            public void onClose(int i, String s, boolean b) {
                Log.i(TAG, "Websocket: Closed " + s);
            }

            @Override
            public void onError(Exception e) {
                Log.i(TAG, "Websocket: Error " + e.getMessage());
            }
        };

        mWebSocketClient.connect();
        startConnectionTimeOut(SERVER_WaiTime);
    }

    private void startConnectionTimeOut(long timeOut) {
        timer = new CountDownTimer(timeOut, timeOut) {
            @Override
            public void onTick(long l) {}

            @Override
            public void onFinish() {
                Log.i(TAG, "onFinish");
                mWebSocketClient.close();
//                mWebSocketClient.onClose(0, "", false);
//                mWebSocketClient.onError(new Exception("Time out"));
//                returnToMainActivity("_speak#hieejn taji ddang cos vaasn ddeef veef keest noosi");
                returnToMainActivity("_speak#b");
            }
        };
        timer.start();
    }

    //-----------------------------------------------------------------
    /**
     * my own stuff
     */

    public void doNaturalLanguageUnderstandingOffline(String sentence) {
        //TODO  fix this fucking error (have to add "  " to first of the sentence if want a precise tagging result)
        String[] words = ("  " + TextUtility.toTelex(sentence)).split(" ");

        String[] tags = ner.tag(words);

        String[] extracted = doInformationExtraction(words, tags);

        returnToMainActivity(extracted[0] + "#" + extracted[1]);
    }

    public void doNaturalLanguageUnderstanding(String sentence) {
        if(OFFLINE_MODE) {
            doNaturalLanguageUnderstandingOffline(sentence);
        } else {
            if(sentence.length() == 0) {
                //fix server 'bugs'???
                returnToMainActivity("#");
            }
            messageToServer = TextUtility.toTelex(sentence);

            connectToUmindServer();
        }
    }

    private String[] doInformationExtraction(String[] words, String[] tags) {
        InformationExtraction ie = new InformationExtraction(words, tags);
        String[] ret = {"", ""};

        for(int i = 0; i < tags.length; i++) {
            switch (tags[i]) {
                case "SET":
                    if(i+1 < tags.length)
                        if(tags[i+1].equals("ALARM")) {
                            ret[0] = "SET_TIME";
                            ret[1] = ie.extractTime("NUMBER", "TIME").replaceAll(" ", "");
                            if(ret[1].equals(""))
                                ret[1] = ie.extractTime("NUMBER_:", "TIME").replaceAll(" ", "");
                            return ret;
                        }
                    break;
                case "ASK":
                    if(i+1 < tags.length)
                        switch (tags[i+1]) {
                            case "TIME":
                                ret[0] = "ASK_TIME";
                                return ret;
                            case "DAY":
                                ret[0] = "ASK_DAY";
                                return ret;
                            case "WEATHER":
                                ret[0] = "ASK_WEATHER";
                                return ret;
                        }
                    break;
                case "DAY":
                    //TODO  get rid of this shit
                    //temporary fix for recognize ASK_DAY command
                    ret[0] = "ASK_DAY";
                    return ret;
                case "OPEN":
                    if(i+1 < tags.length)
                        switch (tags[i+1]) {
                            case "SONG":
                                ret[0] = "PLAY_MUSIC";
                                String extracted = ie.extractTime("O", "PLEASE");
                                extracted = extracted.substring(2, extracted.length()-1);

                                ret[1] = extracted;
                                return ret;
                            case "LIGHT":
                                ret[0] = "SET_HOME";
                                ret[1] = ie.extractTime("O", "PLEASE");
                                return ret;
                        }
                    break;
                case "ADD":
                    ret[0] = "ADD_TODO";
                    ret[1] = ie.extractTime("O", "PLEASE");
                    return ret;
                case "SEARCH":
                    ret[0] = "ASK_SEARCH";
                    ret[1] = ie.extractTime("O", "PLEASE");
                    return ret;

                //temporary fix some command is unrecognized
                case "WEATHER":
                    ret[0] = "ASK_WEATHER";
                    ret[1] = ie.extractTime("DAY", "ASK").replaceAll(":", "");
                    return ret;
                case "HELLO":
                    ret[0] = "HELLO";
                    if(!tags[tags.length-1].equals("O"))
                        ret[0] = tags[tags.length-1];

                    return ret;
                case "BYE":
                    ret[0] = "BYE";

                    return ret;
                case "YOU":
                    ret[0] = "ASK_YOU";

                    return ret;
                case "TRAFFIC":
                    ret[0] = "ASK_TRAFFIC";

                    return ret;
            }
        }

        return ret;
    }

    //-----------------------------------------------------------------
    /**
     * return activity stuff
     */

    public void returnToMainActivity(String message) {
        if (mListener != null) {
            mListener.onNaturalLanguageUnderstandingResult(message);
        }
    }

    public interface onNaturalLanguageUnderstanding {
        void onNaturalLanguageUnderstandingResult(String message);
    }
}
