package org.illustrans.naturallanguageprocessing.speech.speechtotext.androidgoogleapi;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;

import java.util.ArrayList;

public class SpeechToText implements RecognitionListener {

    private static final String TAG = SpeechToText.class.getSimpleName();

    public static final  String EXTRA_MESSAGE = "org.illustrans.MESSAGE";

    private onSpeechToText mListener;

    private Context context;

    //SpeechRecognizer
    private SpeechRecognizer speechRecognizer = null;
    private Intent speechRecognizerIntent;

    //timer
    private CountDownTimer timer = null;

    public SpeechToText(Context c) {
        this.context = c;

        intSpeechRecognition();
    }

    //-----------------------------------------------------------------

    private void intSpeechRecognition() {
        if (context instanceof onSpeechToText) {
            mListener = (onSpeechToText) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement onSpeechToText");
        }

        speechRecognizer = SpeechRecognizer.createSpeechRecognizer(context);
        speechRecognizer.setRecognitionListener(this);
        speechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "vi");
//        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE, "vi");
        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, context.getPackageName());
//        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_WEB_SEARCH);
        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1);
//        speechRecognizer.startListening(speechRecognizerIntent);

        timer = new CountDownTimer(10000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                Log.i("CountDown", "onTick");
            }

            @Override
            public void onFinish() {
                Log.i("CountDown", "onFinish");
//                timer = null;
                returnToMainActivity("");
            }
        };
    }

    public void startSpeechRecognition() {
        if (speechRecognizer == null)
            intSpeechRecognition();

        speechRecognizer.startListening(speechRecognizerIntent);
        startBackupTimer();
    }

    private void startBackupTimer() {
        //in case speech to text stopped for unknown reason
        timer.start();
    }

    @Override
    public void onBeginningOfSpeech() {
        Log.i(TAG, "onBeginningOfSpeech");
    }

    @Override
    public void onBufferReceived(byte[] buffer) {
        Log.i(TAG, "onBufferReceived: " + buffer);
    }

    @Override
    public void onEndOfSpeech() {
        Log.i(TAG, "onEndOfSpeech");
    }

    @Override
    public void onError(int errorCode) {
        String errorMessage = getErrorText(errorCode);
        Log.d(TAG, "FAILED " + errorMessage);

//        speechRecognizer = null;
//        returnToMainActivity("");
    }

    @Override
    public void onEvent(int arg0, Bundle arg1) {
        Log.i(TAG, "onEvent");
    }

    @Override
    public void onPartialResults(Bundle arg0) {
        Log.i(TAG, "onPartialResults");
    }

    @Override
    public void onReadyForSpeech(Bundle arg0) {
        Log.i(TAG, "onReadyForSpeech");
    }

    @Override
    public void onResults(Bundle results) {
        Log.i(TAG, "onResults");
        ArrayList<String> matches = results
                .getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
        String text = "";
        for (String result : matches)
            text += result + "\n";

        Log.i(TAG, "onResults: " + matches.get(0));

        returnToMainActivity(matches.get(0));
    }

    @Override
    public void onRmsChanged(float rmsdB) {

    }

    public static String getErrorText(int errorCode) {
        String message;
        switch (errorCode) {
            case SpeechRecognizer.ERROR_AUDIO:
                message = "Audio recording error";
                break;
            case SpeechRecognizer.ERROR_CLIENT:
                message = "Client side error";
                break;
            case SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS:
                message = "Insufficient permissions";
                break;
            case SpeechRecognizer.ERROR_NETWORK:
                message = "Network error";
                break;
            case SpeechRecognizer.ERROR_NETWORK_TIMEOUT:
                message = "Network timeout";
                break;
            case SpeechRecognizer.ERROR_NO_MATCH:
                message = "No match";
                break;
            case SpeechRecognizer.ERROR_RECOGNIZER_BUSY:
                message = "RecognitionService busy";
                break;
            case SpeechRecognizer.ERROR_SERVER:
                message = "error from server";
                break;
            case SpeechRecognizer.ERROR_SPEECH_TIMEOUT:
                message = "No speech input";
                break;
            default:
                message = "Didn't understand, please try again.";
                break;
        }
        return message;
    }

    //-----------------------------------------------------------------

    public void returnToMainActivity(String message) {
        if (mListener != null) {
            if (timer != null) {
                timer.cancel();
//                timer = null;
            }
            mListener.onSpeechToTextResult(message);
        }
    }

    //-----------------------------------------------------------------
    //interface stuff

    public interface onSpeechToText {
        void onSpeechToTextResult(String message);
    }
}
