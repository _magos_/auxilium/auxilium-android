package org.illustrans.naturallanguageprocessing.speech.wakeupword.kitt.ai.snowboy;

import android.app.IntentService;
import android.content.Intent;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import ai.kitt.snowboy.SnowboyDetect;

/**
 * Created by hpdragon on 31/12/2017.
 */

public class SnowboyWakeUpWordIntentService extends IntentService {

    static {
        System.loadLibrary("snowboy-detect-android");
    }

    private static final String TAG = SnowboyWakeUpWordIntentService.class.getSimpleName();

    private static final String ACTIVE_RES = Constants.ACTIVE_RES;
    private static final String ACTIVE_UMDL = Constants.ACTIVE_UMDL;

    private SnowboyDetect detector;

    private boolean shouldContinue;
    private Thread thread;

    private static String strEnvWorkSpace = Constants.DEFAULT_WORK_SPACE;
    private String activeModel = strEnvWorkSpace+ACTIVE_UMDL;
    private String commonRes = strEnvWorkSpace+ACTIVE_RES;

    public SnowboyWakeUpWordIntentService() {
        super("SnowboyWakeUpWordIntentService");

        detector = new SnowboyDetect(commonRes, activeModel);

        detector.SetSensitivity("0.6");
        //-detector.SetAudioGain(1);
        detector.ApplyFrontend(true);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        startRecording();
    }

    private void sendMessageToReceiver(String message) {
        //do on wake up
        Intent localIntent = new Intent("MSG_ACTIVE").putExtra("abc", true);
        // Broadcasts the Intent to receivers in this app.
        LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
    }

    public void startRecording() {
        if (thread != null)
            return;

        shouldContinue = true;
        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                record();
            }
        });
        thread.start();
    }

    public void stopRecording() {
        if (thread == null)
            return;

        shouldContinue = false;
        thread = null;
    }

    private void record() {
        Log.v(TAG, "Start");
        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_AUDIO);

        // Buffer size in bytes: for 0.1 second of audio
        int bufferSize = (int)(Constants.SAMPLE_RATE * 0.1 * 2);
        if (bufferSize == AudioRecord.ERROR || bufferSize == AudioRecord.ERROR_BAD_VALUE) {
            bufferSize = Constants.SAMPLE_RATE * 2;
        }

        byte[] audioBuffer = new byte[bufferSize];
        AudioRecord record = new AudioRecord(
                MediaRecorder.AudioSource.DEFAULT,
                Constants.SAMPLE_RATE,
                AudioFormat.CHANNEL_IN_MONO,
                AudioFormat.ENCODING_PCM_16BIT,
                bufferSize);

        if (record.getState() != AudioRecord.STATE_INITIALIZED) {
            Log.e(TAG, "Audio Record can't initialize!");
            return;
        }
        record.startRecording();
        Log.v(TAG, "Start recording");

        long shortsRead = 0;
//        detector.Reset();
        while (shouldContinue) {
            record.read(audioBuffer, 0, audioBuffer.length);

            // Converts to short array.
            short[] audioData = new short[audioBuffer.length / 2];
            ByteBuffer.wrap(audioBuffer).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().get(audioData);

            shortsRead += audioData.length;

            // SnowboyWakeUpWord hotword detection.
            int result = detector.RunDetection(audioData, audioData.length);

            if (result == -2) {
                // post a higher CPU usage:
            } else if (result == -1) {
            } else if (result == 0) {
                // post a higher CPU usage:
            } else if (result > 0) {
                Log.i(TAG, "SnowboyWakeUpWord: Hotword " + Integer.toString(result) + " detected!");
                stopRecording();
            }
        }

        record.stop();
        record.release();
        detector.delete();

        Log.v(TAG, String.format("Recording stopped. Samples read: %d", shortsRead));

        sendMessageToReceiver("");
    }
}
