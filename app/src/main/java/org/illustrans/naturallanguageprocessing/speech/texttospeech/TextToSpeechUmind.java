package org.illustrans.naturallanguageprocessing.speech.texttospeech;

/**
 * Created by hpdragon on 26/12/2017.
 */

import android.media.MediaPlayer;
import android.util.Log;

import java.io.IOException;

public class TextToSpeechUmind {

    private static final String TAG = TextToSpeechUmind.class.getSimpleName();

    private TextToSpeechMemory memory;

    private static final String SERVER_URL = "http://speechlab1.uit.edu.vn/tts/tts.php?text=";

    private MediaPlayer mediaPlayer;

    /**
     * constructor
     */
    public TextToSpeechUmind() {
        //memory stuff
        memory = new TextToSpeechMemory();
        memory.setOnSaveSpeechFinish(new TextToSpeechMemory.OnSaveSpeechFinish() {
            @Override
            public void onSaveSpeechFinish(boolean isSaved, String speakSentence) {
                Log.i(TAG, "onSaveSpeechFinish");

                if(isSaved)
                    playSpeechFromFile(speakSentence);
            }
        });

        //music stuff
        mediaPlayer = new MediaPlayer();

        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                //Media palyer will start playing
                mp.start();
            }
        });
        mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                //is not able to play file
                return false;
            }
        });
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mediaPlayer.stop();
            }
        });
    }

    /**
     * destructor
     */
    public void delete() {
        mediaPlayer.stop();
        mediaPlayer.release();
    }

    /**
     * speak the speakSentence
     * note: this is a nonblocking method
     * @param   speakSentence   sentence to be spoken
     */
    public void speak(String speakSentence) {
        if(!memory.checkMemory(speakSentence)) {
            memory.saveSpeechToFile(speakSentence, SERVER_URL + speakSentence.replace(" ", "%20"));
        } else
            playSpeechFromFile(speakSentence);
    }

    private void playSpeechFromFile(String speakSentence) {
        if (speakSentence == null)
            return;

        if (mediaPlayer.isPlaying())
            mediaPlayer.stop();
        mediaPlayer.reset();

        try {
            String fileLocation = memory.getMemoryLocation(speakSentence);
            if(fileLocation.length() > 0) {
                mediaPlayer.setDataSource(fileLocation);
                mediaPlayer.prepare();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
