package org.illustrans.naturallanguageprocessing.speech.texttospeech;

import android.os.Environment;

import java.io.File;

public class Constants {
    public static final String ASSETS_RES_DIR = "texttospeech";
    public static final String DEFAULT_WORK_SPACE = Environment.getExternalStorageDirectory().getAbsolutePath() + "/texttospeech/";
}
