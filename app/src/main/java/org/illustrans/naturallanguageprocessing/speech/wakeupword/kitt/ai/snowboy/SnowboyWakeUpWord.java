package org.illustrans.naturallanguageprocessing.speech.wakeupword.kitt.ai.snowboy;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

public class SnowboyWakeUpWord {

    private static final String TAG = SnowboyWakeUpWord.class.getSimpleName();

    private Context context;

    private onWakeUpWord mListener;

    //service stuff
    private Intent serviceIntent;
    private IntentFilter statusIntentFilter;
    private BroadcastReceiver broadcastReceiver;

    //-----------------------------------------------------------------
    /*
    constructor stuff
     */

    public SnowboyWakeUpWord(Context c) {
        context = c;

        if (context instanceof onWakeUpWord) {
            mListener = (onWakeUpWord) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement onSpeechToText");
        }

        //resources stuff
//        AppResCopy.copyResFromAssetsToSD(this, Constants.ASSETS_RES_DIR);

        //sign up for broad cast receiver
        initBroadcastReceiver();

        //service stuff
        serviceIntent = new Intent(context, SnowboyWakeUpWordIntentService.class);
    }

    private void initBroadcastReceiver() {
        // The filter's action is BROADCAST_ACTION
        statusIntentFilter = new IntentFilter("MSG_ACTIVE");

        //receiver
        broadcastReceiver = new BroadcastReceiver() {
            private final String TAG = SnowboyWakeUpWord.TAG + "-" + BroadcastReceiver.class.getSimpleName();

            @Override
            public void onReceive(Context context, Intent intent) {
                Log.i(TAG, "onReceive");
                returnToMainActivity();
            }
        };

        // Registers the DownloadStateReceiver and its intent filters
        LocalBroadcastManager.getInstance(context).registerReceiver(broadcastReceiver, statusIntentFilter);
    }

    //-----------------------------------------------------------------
    /*
    wake up word stuff
     */

    public void start() {
        context.startService(serviceIntent);
    }

    //-----------------------------------------------------------------

    public void returnToMainActivity() {
        if (mListener != null) {
            mListener.onWakeUpWordResult();
        }
    }

    public interface onWakeUpWord {
        void onWakeUpWordResult();
    }
}
