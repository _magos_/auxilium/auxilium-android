package org.illustrans.naturallanguageprocessing.speech.texttospeech;

import android.util.Log;

import org.illustrans.auxilium.util.AppResCopy;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;

/**
 * Created by hpdragon on 04/01/2018.
 */

class TextToSpeechMemory {

    private static final String TAG = TextToSpeechMemory.class.getSimpleName();

    private OnSaveSpeechFinish onSaveSpeechFinish;

    private static final String TEXTTOSPEECH_MEMORY_PATH = AppResCopy.DEFAULT_WORK_SPACE + Constants.ASSETS_RES_DIR + "/";
    private static final String TEXTTOSPEECH_MEMORY_MAP_FILE_PATH = TEXTTOSPEECH_MEMORY_PATH + "memory.tts";

    private Hashtable<String, String> memoryMap;
    private static final String SEPARATOR = "__";

    private static final int BUFFEREDREADER_SIZE = 256;

    public TextToSpeechMemory() {
        memoryMap = new Hashtable<>();

        try {
            File checkFile = new File(TEXTTOSPEECH_MEMORY_MAP_FILE_PATH);
            if(!checkFile.exists())
                checkFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            FileReader memoryFile = new FileReader(TEXTTOSPEECH_MEMORY_MAP_FILE_PATH);
            BufferedReader bufferedReader = new BufferedReader(memoryFile);

            String line;
            while((line = bufferedReader.readLine()) != null) {
                Log.i(TAG, line);
                String[] data = line.split(SEPARATOR);
                if(data.length > 1)
                    memoryMap.put(data[0], data[1]);
            }
            bufferedReader.close();
            memoryFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void wipeMemory() {
        File needToWipe = new File(TEXTTOSPEECH_MEMORY_PATH);
        if(!needToWipe.exists())
            return;
        File[] files = needToWipe.listFiles();
        if(files == null)
            return;
        for(File file: files) {
            if(file.isFile())
                file.delete();
        }
    }

    public boolean checkMemory(String speakSentence) {
        String fileName = memoryMap.get(speakSentence);
        if(fileName != null)
            return true;
        return false;
    }

    public void addNewMemory(String speakSentence) {
        try {
            FileWriter memoryFile = new FileWriter(TEXTTOSPEECH_MEMORY_MAP_FILE_PATH, true);
            BufferedWriter bufferedWriter = new BufferedWriter(memoryFile);
            PrintWriter printWriter = new PrintWriter(bufferedWriter);

            memoryMap.put(speakSentence, "");
            for(Map.Entry m : memoryMap.entrySet()) {
                if(m.getKey().equals(speakSentence)) {
                    memoryMap.put(speakSentence, String.valueOf(m.hashCode()));
                    printWriter.println(m.getKey() + SEPARATOR + m.getValue());
                }
            }

            printWriter.close();
            bufferedWriter.close();
            memoryFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getMemoryLocation(String speakSentence) {
        String ret = memoryMap.get(speakSentence);
        if(ret != null)
            return TEXTTOSPEECH_MEMORY_PATH + ret;
        return "";
    }

    public void saveSpeechToFile(String speakSentence, String speechUrl) {
        addNewMemory(speakSentence);

        final String speakSentenceTemp = speakSentence;
        final String speechUrlTemp = speechUrl;
        new Thread() {
            @Override
            public void run() {
                try {
                    URL url = new URL(speechUrlTemp);
                    InputStream is = url.openStream();

                    File file = new File(getMemoryLocation(speakSentenceTemp));
                    if(file.exists())
                        file.createNewFile();
                    FileOutputStream fos = new FileOutputStream(file);
                    int bytesRead = -1;
                    byte[] buffer = new byte[BUFFEREDREADER_SIZE];
                    while ((bytesRead = is.read(buffer)) != -1)
                        fos.write(buffer);

                    fos.close();
                    is.close();

                    if(onSaveSpeechFinish != null)
                        onSaveSpeechFinish.onSaveSpeechFinish(true, speakSentenceTemp);
                } catch (IOException e) {
                    e.printStackTrace();
                    if(onSaveSpeechFinish != null)
                        onSaveSpeechFinish.onSaveSpeechFinish(false, "");
                }
            }
        }.start();
    }

    public interface OnSaveSpeechFinish {
        void onSaveSpeechFinish(boolean isSaved, String speakSentence);
    }

    public void setOnSaveSpeechFinish(OnSaveSpeechFinish on) {
        onSaveSpeechFinish = on;
    }
}
