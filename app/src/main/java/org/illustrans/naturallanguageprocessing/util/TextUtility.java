package org.illustrans.naturallanguageprocessing.util;

import android.net.Uri;

import java.util.Arrays;
import java.util.List;

import static java.lang.Math.max;

public class TextUtility {

    public static String toTelex(String from) {

        String[] ori = {"ố", "ồ", "ổ", "ộ", "ỗ", "ế", "ắ", "ằ", "ẳ", "ẵ", "ặ",
                "ấ", "ẩ", "ẫ", "ầ", "ậ", "ớ", "ờ", "ở", "ỡ", "ề", "ể", "ễ",
                "ệ", "ứ", "ừ", "ử", "ữ", "ự", "ợ", "ý", "ỳ", "ỷ", "ỹ", "ỵ",
                "í", "ì", "ị", "ỉ", "ĩ", "ê", "é", "è", "ẻ", "ẽ", "ẹ", "ư",
                "â", "ơ", "ă", "á", "à", "ả", "ã", "ạ", "ú", "ù", "ủ", "ũ",
                "ụ", "ô", "ó", "ò", "ỏ", "õ", "ọ", "đ"};
        String[] neww = {"oos", "oof", "oor", "ooj", "oox", "ees", "aws",
                "awf", "awr", "awx", "awj", "aas", "aar", "aax", "aaf", "aaj",
                "ows", "owf", "owr", "owx", "eef", "eer", "eex", "eej", "uws",
                "uwf", "uwr", "uwx", "uwj", "owj", "ys", "yf", "yr", "yx",
                "yj", "is", "if", "ij", "ir", "ix", "ee", "es", "ef", "er",
                "ex", "ej", "uw", "aa", "ow", "aw", "as", "af", "ar", "ax",
                "aj", "us", "uf", "ur", "ux", "uj", "oo", "os", "of", "or",
                "ox", "oj", "dd"};
        String result = from.toLowerCase();
        for (int i = 0; i < ori.length; i++) {

            result = result.replace(ori[i], neww[i]);
        }

        return result;
    }

    public static String toTextToSent(String from) {
        String[] ori = {"ố", "ồ", "ổ", "ộ", "ỗ", "ế", "ắ", "ằ", "ẳ", "ẵ", "ặ",
                "ấ", "ẩ", "ẫ", "ầ", "ậ", "ớ", "ờ", "ở", "ỡ", "ề", "ể", "ễ",
                "ệ", "ứ", "ừ", "ử", "ữ", "ự", "ợ", "ý", "ỳ", "ỷ", "ỹ", "ỵ",
                "í", "ì", "ị", "ỉ", "ĩ", "ê", "é", "è", "ẻ", "ẽ", "ẹ", "ư",
                "â", "ơ", "ă", "á", "à", "ả", "ã", "ạ", "ú", "ù", "ủ", "ũ",
                "ụ", "ô", "ó", "ò", "ỏ", "õ", "ọ", "đ"};
        String[] neww = {"oos", "oof", "oor", "ooj", "oox", "ees", "aws",
                "awf", "awr", "awx", "awj", "aas", "aar", "aax", "aaf", "aaj",
                "ows", "owf", "owr", "owx", "eef", "eer", "eex", "eej", "uws",
                "uwf", "uwr", "uwx", "uwj", "owj", "ys", "yf", "yr", "yx",
                "yj", "is", "if", "ij", "ir", "ix", "ee", "es", "ef", "er",
                "ex", "ej", "uw", "aa", "ow", "aw", "as", "af", "ar", "ax",
                "aj", "us", "uf", "ur", "ux", "uj", "oo", "os", "of", "or",
                "ox", "oj", "dd"};
        String result = from.toLowerCase();
        for (int i = 0; i < ori.length; i++) {

            result = result.replace(ori[i], neww[i].toUpperCase());
        }
        return result;
    }

    public static String encodeToURL(String from) {
        return Uri.encode(from);
    }

    public static String decodeToURL(String from) {
        return Uri.decode(from);
    }

    public static String toTextFromRe(String from) {
        String[] ori = {"ố", "ồ", "ổ", "ộ", "ỗ", "ế", "ắ", "ằ", "ẳ", "ẵ", "ặ",
                "ấ", "ẩ", "ẫ", "ầ", "ậ", "ớ", "ờ", "ở", "ỡ", "ề", "ể", "ễ",
                "ệ", "ứ", "ừ", "ử", "ữ", "ự", "ợ", "ý", "ỳ", "ỷ", "ỹ", "ỵ",
                "í", "ì", "ị", "ỉ", "ĩ", "ê", "é", "è", "ẻ", "ẽ", "ẹ", "ư",
                "â", "ơ", "ă", "á", "à", "ả", "ã", "ạ", "ú", "ù", "ủ", "ũ",
                "ụ", "ô", "ó", "ò", "ỏ", "õ", "ọ", "đ"};
        String[] newWord = {"oos", "oof", "oor", "ooj", "oox", "ees", "aws",
                "awf", "awr", "awx", "awj", "aas", "aar", "aax", "aaf", "aaj",
                "ows", "owf", "owr", "owx", "eef", "eer", "eex", "eej", "uws",
                "uwf", "uwr", "uwx", "uwj", "owj", "ys", "yf", "yr", "yx",
                "yj", "is", "if", "ij", "ir", "ix", "ee", "es", "ef", "er",
                "ex", "ej", "uw", "aa", "ow", "aw", "as", "af", "ar", "ax",
                "aj", "us", "uf", "ur", "ux", "uj", "oo", "os", "of", "or",
                "ox", "oj", "dd"};
        String result = from.toLowerCase();
        for (int i = 0; i < ori.length; i++) {

            result = result.replace(newWord[i].toUpperCase(), ori[i]);
        }
        return result;
    }

    public static String toVNnonSign(String from) {

        String[] ori = {"oos", "oof", "oor", "ooj", "oox", "ees", "aws",
                "awf", "awr", "awx", "awj", "aas", "aar", "aax", "aaf", "aaj",
                "ows", "owf", "owr", "owx", "eef", "eer", "eex", "eej", "uws",
                "uwf", "uwr", "uwx", "uwj", "owj", "ys", "yf", "yr", "yx",
                "yj", "is", "if", "ij", "ir", "ix", "ee", "es", "ef", "er",
                "ex", "ej", "uw", "aa", "ow", "aw", "as", "af", "ar", "ax",
                "aj", "us", "uf", "ur", "ux", "uj", "oo", "os", "of", "or",
                "ox", "oj", "dd"};
        String[] newWord = {"o", "o", "o", "o", "o", "e", "a",
                "a", "a", "a", "a", "a", "a", "a", "a", "a",
                "o", "o", "o", "o", "e", "e", "e", "e", "u",
                "u", "u", "u", "u", "o", "y", "y", "y", "y",
                "y", "i", "i", "i", "i", "i", "e", "e", "e", "e",
                "e", "e", "u", "a", "o", "a", "a", "a", "a", "a",
                "a", "u", "u", "u", "u", "u", "o", "o", "o", "o",
                "o", "o", "d"};
        String result = from.toLowerCase();
        for (int i = 0; i < ori.length; i++) {

            result = result.replace(ori[i], newWord[i]);
        }

        return result;
    }

    public static String toVN(String from) {

        String[] ori = {"ố", "ồ", "ổ", "ộ", "ỗ", "ế", "ắ", "ằ", "ẳ", "ẵ", "ặ",
                "ấ", "ẩ", "ẫ", "ầ", "ậ", "ớ", "ờ", "ở", "ỡ", "ề", "ể", "ễ",
                "ệ", "ứ", "ừ", "ử", "ữ", "ự", "ợ", "ý", "ỳ", "ỷ", "ỹ", "ỵ",
                "í", "ì", "ị", "ỉ", "ĩ", "ê", "é", "è", "ẻ", "ẽ", "ẹ", "ư",
                "â", "ơ", "ă", "á", "à", "ả", "ã", "ạ", "ú", "ù", "ủ", "ũ",
                "ụ", "ô", "ó", "ò", "ỏ", "õ", "ọ", "đ"};
        String[] newWord = {"oos", "oof", "oor", "ooj", "oox", "ees", "aws",
                "awf", "awr", "awx", "awj", "aas", "aar", "aax", "aaf", "aaj",
                "ows", "owf", "owr", "owx", "eef", "eer", "eex", "eej", "uws",
                "uwf", "uwr", "uwx", "uwj", "owj", "ys", "yf", "yr", "yx",
                "yj", "is", "if", "ij", "ir", "ix", "ee", "es", "ef", "er",
                "ex", "ej", "uw", "aa", "ow", "aw", "as", "af", "ar", "ax",
                "aj", "us", "uf", "ur", "ux", "uj", "oo", "os", "of", "or",
                "ox", "oj", "dd"};
        String result = from.toLowerCase();
        for (int i = 0; i < ori.length; i++) {

            result = result.replace(newWord[i], ori[i]);
        }

        return result;
    }

    private static boolean isNumeric(String str) {
        for (char c : str.toCharArray()) {
            if (!Character.isDigit(c)) return false;
        }
        return true;
    }

    public static String ConCateString(String msg) {
        String[] arrS = msg.split(" ");
        StringBuilder k = new StringBuilder();
        boolean lastIsNumeric = true;
        for (String s : arrS) {
            if (lastIsNumeric && isNumeric(s))
                k.append(s);
            else k.append(" ").append(s);
            lastIsNumeric = isNumeric(s);
        }
        return k.toString().trim();
    }

    private static String[] vnKey = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", " ", "ố", "ồ", "ổ", "ộ", "ỗ", "ế", "ắ", "ằ", "ẳ", "ẵ", "ặ",
            "ấ", "ẩ", "ẫ", "ầ", "ậ", "ớ", "ờ", "ở", "ỡ", "ề", "ể", "ễ",
            "ệ", "ứ", "ừ", "ử", "ữ", "ự", "ợ", "ý", "ỳ", "ỷ", "ỹ", "ỵ",
            "í", "ì", "ị", "ỉ", "ĩ", "ê", "é", "è", "ẻ", "ẽ", "ẹ", "ư",
            "â", "ơ", "ă", "á", "à", "ả", "ã", "ạ", "ú", "ù", "ủ", "ũ",
            "ụ", "ô", "ó", "ò", "ỏ", "õ", "ọ", "đ", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};

    public static int StringInStringPoint(String a, String b) {
        StringBuilder la = new StringBuilder();
        StringBuilder lb = new StringBuilder();


        List<String> chs = Arrays.asList(vnKey);
        for (char ch : a.toLowerCase().toCharArray()) {
            if (chs.contains(String.valueOf(ch))) {
                la.append(ch);
            } else la.append(" ");
        }

        for (char ch : b.toLowerCase().toCharArray()) {
            if (chs.contains(String.valueOf(ch))) {
                lb.append(ch);
            } else lb.append(" ");
        }

        while (la.toString().contains("  "))
            la = new StringBuilder(la.toString().replace("  ", " "));
        while (lb.toString().contains("  "))
            lb = new StringBuilder(lb.toString().replace("  ", " "));

        String[] ea = la.toString().trim().split(" ");
        String[] eb = lb.toString().trim().split(" ");

        int[][] qhd = new int[ea.length][eb.length];

        for (int i = 0; i < ea.length; i++) {
            if (ea[i].equals(eb[0]))
                qhd[i][0] = 1;
            else qhd[i][0] = 0;
        }
        for (int i = 0; i < eb.length; i++) {
            if (ea[0].equals(eb[i]))
                qhd[0][i] = 1;
            else qhd[0][i] = 0;
        }

        int maxX = 0;
        for (int i = 1; i < ea.length; i++)
            for (int j = 1; j < eb.length; j++) {
                int diem = 0;
                if (ea[i].equals(eb[j])) {
                    if (ea[i - 1].equals(eb[j - 1])) {
                        diem += 5;
                    } else diem += 2;
                }
                qhd[i][j] = max(max(diem + qhd[i - 1][j - 1], qhd[i - 1][j]), qhd[i][j - 1]);
                if (qhd[i][j] > maxX) maxX = qhd[i][j];
            }
        return maxX;
    }
}
