package org.illustrans.cognitive.reaction;

import android.util.Log;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by hpdragon on 03/01/2018.
 */

public class Reaction {

    private static final String TAG = Reaction.class.getSimpleName();

    public Reaction() {

    }

    public String[] doReflectReaction(String command) {
        String[] empty = {"", ""};
        if(!(command.length() > 0))
            return empty;

        //check if first char is '_', 'I'(wtf -_-) (offline NLU) or else (online NLU)
        if( command.charAt(0) != '_' && command.charAt(0) != 'I') {
            return handleOfflineCommand(command);
        } else {
            return handleOnlineCommand(command);
        }
    }

    private String[] handleOnlineCommand(String command) {
        String[] empty = {"", ""};
        if(!(command.length() > 0))
            return empty;

        String[] reaction = command.split("#");
        switch (reaction.length) {
            case 0:
                Log.i(TAG + " " + "Reaction", "NULL 2");
                empty[0] = "_dontknow";
                return empty;
            case 1:
                Log.i(TAG + " " + "Reaction", "NULL 3");
                empty[0] = "_notenough";
                return empty;
            default:
                break;
        }

        return reaction;
    }

    private String[] handleOfflineCommand(String command) {
        String[] ret = {"_dontknow", ""};
        if(!(command.length() > 0))
            return ret;

        String[] reaction = command.split("#");
        if(reaction.length < 1)
            return ret;

        switch (reaction[0]) {
            case "SET_TIME":
                ret[0] = "_alarmset";
                if(reaction.length == 2) {
                    ret[1] = reaction[1];
                    if(reaction[1].endsWith(":"))
                        ret[1] = reaction[1].concat("0");
                }
                else
                    ret[0] = "_notenough";
                break;
            case "ASK_TIME":
                Date time = Calendar.getInstance().getTime();
                ret[0] = "_speak";
                ret[1] = "baay giowf laf " + time.getHours() + " giowf " + time.getMinutes() + " phust";

                break;
            case "ASK_DAY":
                Calendar calendar = Calendar.getInstance();
                ret[0] = "_speak";
                //FIXME (day.get(Calendar.MONTH)+1) ???
                ret[1] = "ngafy hoom nay laf ngafy " + calendar.get(Calendar.DAY_OF_MONTH) + " thasng " + (calendar.get(Calendar.MONTH)+1);

                break;
            case "ASK_WEATHER":
                ret[0] = "_weather";
                break;
            case "PLAY_MUSIC":
                ret[0] = "_music";
                ret[1] = reaction[1];
                break;
            case "SET_HOME":
                ret[0] = "IOTHOME";
                break;
            case "ADD_TODO":
                ret[0] = "_addtodo";
                ret[1] = reaction[1];
                break;
            case "ASK_SEARCH":
                ret[0] = "_search";
                ret[1] = reaction[1];
                break;
            case "HELLO":
                ret[0] = "_speak";
                ret[1] = "xin chafo bajn";

                break;
            case "BYE":
                ret[0] = "_speak";
                ret[1] = "chafo bajn";

                break;
            case "ASK_YOU":
                ret[0] = "_speak";
                ret[1] = "tooi raast khoer";

                break;
            case "ASK_TRAFFIC":
                ret[0] = "_speak";
                ret[1] = "hoom nay dduwowfng raast ddoong";

                break;
            default:
                ret[0] = "_dontknow";
                break;
        }

        return ret;
    }
}
